# Build

First copy **MXE** dir to this folder.

```dockerfile
docker build -t srteam/mxecap:latest .
```

# Usage

```dockerfile
docker pull srteam/mxecap:latest
```

[dockerhub](https://hub.docker.com/repository/docker/srteam/mxecap)

## Current image

### Packages

- gcc
- boost
- cmake
- qtbase
- qtmultimedia
- lua
- libzip
- lz4
- lzo
- openssl

### Manual installed

- capstone
