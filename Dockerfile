FROM archlinux:latest

RUN pacman -Sy && pacman -S --noconfirm binutils make upx flex && pacman -Scc --noconfirm && rm -rf /var/cache/pacman/pkg/

COPY mxe /mxe

ENV PATH "/mxe/usr/bin:$PATH"
